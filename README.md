<div align="center">
  <h1>FinSom is FindSomebody</h1>
  <p>
    <strong>An application that will help you find a specific person.</strong>
  </p>

<p>
[![Release](https://img.shields.io/badge/release-none-red)](https://codeberg.org/nanoory/FinSom/releases)
![CSharp Dotnet](https://img.shields.io/badge/CSharp_Dotnet-v6.0-purple)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
</p>

</div>


## Table of content

- [About](#about)
- [Screenshots](#screenshots)
- [Features](#features)
- [Operating systems](#operating-systems)
- [Built with](#built-with)
  - [Building](#building)
- [TODO](#todo)
- [Licensing](#licensing)

# About

> About the project

The aim of the project is to create an application for OSINT.
Main activity will be to search specific internet sources to download files such as for eg. PDF and search them for a specific person.

Please note that some application features may lead to server problems and may expose you to law consequences.
The application should be considered as developed for educational purposes only.

# Screenshots

# Features

Download PDF files from various sources and search them for a specific person.

# Operating systems

> Program tests on operating systems and availability in the official repository

| Operating system | Tested | Repository | Link |
| ---------------- | ------ | ---------- | ---- |
| Debian Bullseye  | Yes    |
| BlackArch        | No     |

# Built with

- C# Dotnet 6.0

## Building

> How to build

```
git clone https://codeberg.org/nanoory/FinSom
dotnet build
```

# TODO

- [x] Base functions

# Licensing

GPL-3.0 only
